document.querySelector('header').addEventListener('click', function () {
    this.style.backgroundColor = 'red';
});

const imagenes = document.querySelectorAll("#experiencia img");
let index = 0;

function cambiarImagen() {
    imagenes[index].style.transform = "scale(1.2)"; /* Aumenta el tamaño de la imagen actual */
    setTimeout(() => {
        imagenes[index].style.transform = "scale(1)"; /* Vuelve a tamaño normal */
        index = (index + 1) % imagenes.length; /* Cambia al siguiente índice de imagen */
        setTimeout(cambiarImagen, 2000); /* Llama a la función nuevamente después de 2 segundos */
    }, 1000); /* La imagen se agranda durante 1 segundo antes de cambiar */
}

cambiarImagen(); /* Comienza la animación al cargar la página */
